<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class Presentation extends AbstractController
{
  /**
   * @Route("/pres", name="presentation") 
   */
  public function pres()
  {
    $nom = "Munoz";
    $prénom = "Berta";
    $tab = ["1", "2", "3", "4", "5", "6", "7"];
    $tabtab = ["un", "deux", "trois", "quatre", "cinq", "six", "sept"];

    return $this->render(
      "presentation.html.twig",
      [
        "phrase" => "Bonjour, je suis ",
        "firstname" => $prénom,
        "name" => $nom,
        "tableau" => $tab, // on expose le tableau
        "tablo" => $tabtab
      ]
    );
  }

// public function expo(){

 
  
//   return $this->render("presentation.html.twig", []);
// }
}
 

// Mini Tp Pair/Impair
// 1. Dans le contrôleur PresentationController, exposer un tableau d'éléments
// NOUVEAUX MESSAGES
// Jean Simplon - Aujourd'hui à 14:57
// 2. Dans le template presentation.twig.html, faire une boucle sur le tableau
// 3. En utilisant un if-else, faire en sorte que les éléments pair soit affichés en vert et les éléments impairs en rouge




