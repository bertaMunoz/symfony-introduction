<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Person;
use Symfony\Component\HttpFoundation\Request;


class AddController extends AbstractController{

  /**
   * @Route ("/add-person", name = "add_person")
   */

   public function index(){ // pas de request parce que ce n'est pas lui qui se charge de la soumission


    return $this->render("addPerson.html.twig",[]) ;    
    
   }

    /**
     * @Route ("/add-person-action", name = "addPerson")
     */

   public function addPerson(Request $req){ // une seule request pour tous les arguments

    $name = $req->get("name");
    $surname = $req->get("surname");
    $age = $req->get("age");

    $person = New Person($name, $surname, $age);  // mettre le new Person après les req 



    return $this->render("addPerson.html.twig",["person" => $person]) ;

   }
}


    // $pers = new Person($req->get("name"),$req->get("surname"),$req->get("age"));
