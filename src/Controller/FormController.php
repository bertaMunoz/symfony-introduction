<?php

namespace  App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class FormController extends AbstractController{
  /**
   * @Route("/form", name = "form" )
   */

   public function index(Request $request){ //1er argument: Typde l'Objet Request  // 2è argument : nom de la variable.
      $requestInput = $request->get("input"); // méthode get appelée sur la variable $request // "input": nom de l'input. 

    return $this->render("formController.html.twig", ["variableInput" => $requestInput]);
      
   }
}



// 1. Faire un nouveau controleur FormController, avec une route "/form"
// 2. Faire que le template de ce contrôleur hérite du template skeleton.html.twig
// 3. Dans le block content du template, faire un formulaire avec un input type text et un button
