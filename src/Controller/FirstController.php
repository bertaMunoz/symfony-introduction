<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Person;


class FirstController extends AbstractController {
/**
 * @Route("/", name="homepage") 
 */
  public function index(){
    $nombre=5;
    $tableau = ["ga","zo","bu"];

    $instance = new Person("Simplon", "Jose", 45); // ici on crée une instance de person déclarée dans la class person

    return $this->render("first.html.twig", ["variable" => "je viens du controller",
    "autre" => $nombre, "tableau" => $tableau, "person"=> $instance]);
  }
  /**
   * @Route("/test", name = "test")
   */
  public function test(){
    return new Response("test");
  }
}
