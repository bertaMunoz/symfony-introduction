<?php

namespace  App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TemplateController extends AbstractController {

  /**
   * @Route("/template", name = "template")
   */
 public function index(){

  return $this->render("template.html.twig", []);
  }
}